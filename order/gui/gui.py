# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jul 21 2013)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
from wx.lib.masked.numctrl import NumCtrl, EVT_NUM
from ObjectListView import ObjectListView

###########################################################################
## Class OrderPanel
###########################################################################

class OrderPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.pnlSidebar = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer1.Add( self.pnlSidebar, 4, wx.EXPAND, 5 )
		
		self.pnlMain = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer1.Add( self.pnlMain, 6, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
	
	def __del__( self ):
		pass
	

###########################################################################
## Class OrderListPanel
###########################################################################

class OrderListPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
		
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		fgSizer1 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer1.AddGrowableCol( 1 )
		fgSizer1.SetFlexibleDirection( wx.BOTH )
		fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Meja", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )
		fgSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )
		
		self.lblMeja = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lblMeja.Wrap( -1 )
		fgSizer1.Add( self.lblMeja, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"Waktu", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )
		fgSizer1.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		self.lblWaktu = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lblWaktu.Wrap( -1 )
		fgSizer1.Add( self.lblWaktu, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"Server", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		fgSizer1.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.lblServer = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lblServer.Wrap( -1 )
		fgSizer1.Add( self.lblServer, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText7 = wx.StaticText( self, wx.ID_ANY, u"Pax", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		fgSizer1.Add( self.m_staticText7, 0, wx.ALL, 5 )
		
		self.numPax = NumCtrl(self, value=1, allowNegative=False, groupDigits=True, min=1, max=999, limitOnFieldChange=True, selectOnEntry=True, autoSize=True)
		fgSizer1.Add( self.numPax, 1, wx.ALL, 5 )
		
		
		bSizer2.Add( fgSizer1, 0, wx.EXPAND, 5 )
		
		self.olvOrder = ObjectListView(self, wx.ID_ANY, style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer2.Add( self.olvOrder, 1, wx.ALL|wx.EXPAND, 5 )
		
		fgSizer2 = wx.FlexGridSizer( 4, 2, 0, 0 )
		fgSizer2.AddGrowableCol( 0 )
		fgSizer2.SetFlexibleDirection( wx.BOTH )
		fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText8 = wx.StaticText( self, wx.ID_ANY, u"Total", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText8.Wrap( -1 )
		fgSizer2.Add( self.m_staticText8, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.numTotal = NumCtrl(self, value=0, allowNegative=False, groupDigits=True, min=0, limitOnFieldChange=True, selectOnEntry=True, autoSize=True)
		fgSizer2.Add( self.numTotal, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText9 = wx.StaticText( self, wx.ID_ANY, u"Discount", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText9.Wrap( -1 )
		fgSizer2.Add( self.m_staticText9, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.numDiscount = NumCtrl(self, value=0, allowNegative=False, groupDigits=True, min=0, limitOnFieldChange=True, selectOnEntry=True, autoSize=True)
		fgSizer2.Add( self.numDiscount, 0, wx.ALL, 5 )
		
		self.m_staticText10 = wx.StaticText( self, wx.ID_ANY, u"Pajak", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText10.Wrap( -1 )
		fgSizer2.Add( self.m_staticText10, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.numPajak = NumCtrl(self, value=0, allowNegative=False, groupDigits=True, min=0, limitOnFieldChange=True, selectOnEntry=True, autoSize=True)
		fgSizer2.Add( self.numPajak, 0, wx.ALL, 5 )
		
		self.m_staticText11 = wx.StaticText( self, wx.ID_ANY, u"Grand Total", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_RIGHT )
		self.m_staticText11.Wrap( -1 )
		fgSizer2.Add( self.m_staticText11, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.numGrandTotal = NumCtrl(self, value=0, allowNegative=False, groupDigits=True, min=0, limitOnFieldChange=True, selectOnEntry=True, autoSize=True)
		fgSizer2.Add( self.numGrandTotal, 0, wx.ALL, 5 )
		
		
		bSizer2.Add( fgSizer2, 0, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer2 )
		self.Layout()
	
	def __del__( self ):
		pass
	

