import wx

import copy

from datetime import datetime
from dateutil import tz

import logging
import traceback

import ObjectListView
from ObjectListView import ColumnDefn

import gui

from rest import api as rest

from wx.lib.masked.numctrl import EVT_NUM


utcnow = lambda: datetime.now(tz=tz.tzutc())


def fillEmptySpaces(sizer, spaces):
    for s in xrange(spaces):
        sizer.AddStretchSpacer()


class OrderPanel(gui.OrderPanel):
    def __init__(self, parent, serverurl='http://localhost:5000/api'):
        super(OrderPanel, self).__init__(parent)
        self.serverurl = serverurl

        self.showMejaPanel()

    def __clearSidebarPanel(self):
        sizer = self.pnlSidebar.GetSizer()

        if sizer is None:
            return

        sizer.DeleteWindows()

    def __clearMainPanel(self):
        sizer = self.pnlMain.GetSizer()

        if sizer is None:
            return

        sizer.DeleteWindows()

    def showMejaPanel(self):
        self.__clearMainPanel()
        self.pnlSidebar.Hide()

        pnl = MejaPanel(self.pnlMain, serverurl=self.serverurl)
        pnl.Bind(wx.EVT_BUTTON, self.onMejaClicked)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pnl, 1, wx.EXPAND | wx.ALL, 5)

        self.pnlMain.SetSizerAndFit(sizer)

        self.__clearSidebarPanel()

        self.Layout()

    def showMenuPanel(self, meja):
        self.__clearMainPanel()
        self.pnlSidebar.Show()

        pnl = MenuPanel(self.pnlMain, serverurl=self.serverurl)
        pnl.Bind(wx.EVT_BUTTON, self.onMenuClicked)

        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        buttonTutup = wx.Button(self.pnlMain, wx.ID_ANY, '&Tutup')
        buttonTutup.Bind(wx.EVT_BUTTON, self.onMenuClose)
        buttonPesan = wx.Button(self.pnlMain, wx.ID_ANY, '&Pesan')
        buttonPesan.Bind(wx.EVT_BUTTON, self.onPesanClicked)
        buttonSizer.Add(buttonTutup, 0, wx.EXPAND | wx.ALL, 5)
        buttonSizer.Add(buttonPesan, 0, wx.EXPAND | wx.ALL, 5)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pnl, 1, wx.EXPAND | wx.ALL, 5)
        sizer.Add(buttonSizer, 0, wx.EXPAND | wx.ALL, 5)
        self.pnlMain.SetSizerAndFit(sizer)

        self.__clearSidebarPanel()

        try:
            orderID = meja.order_ids[0]
            #order = rest.getOrder(serverurl=self.serverurl, 
            #        objId=orderID).json()
            order = rest.getOrder(serverurl=self.serverurl,
                    objId=orderID)
        except (KeyError, IndexError) as e:
            order = None

        #self.orderPanel = OrderListPanel(self, 
        #        items=order['items'] if order is not None else None)
        self.orderPanel = OrderListPanel(self,
                items=order.items if order is not None else None)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.orderPanel, 1, wx.EXPAND | wx.ALL, 5)
        self.pnlSidebar.SetSizerAndFit(sizer)

        self.Layout()

    def onMenuClicked(self, event):
        obj = event.GetEventObject()

        item = obj.userdata
        item.qty = 1
        item.subtotal = item.qty * item.harga

        self.orderPanel.addItem(obj.userdata)

    def onMejaClicked(self, event):
        self.showMenuPanel(event.GetEventObject().userdata)

    def onMenuClose(self, event):
        self.showMejaPanel()

    def onPesanClicked(self, event):
        print 'pesan'


class MejaPanel(wx.Panel):
    def __init__(self, parent, serverurl='http://localhost:5000/api'):
        super(MejaPanel, self).__init__(parent)

        self.serverurl = serverurl
        self.objects_per_page = 50

        self.refresh()

    def createButtons(self, objects, parent, sizer):
        for obj in objects:
            button = wx.Button(parent, wx.ID_ANY, label=obj.nama)
            button.userdata = obj
            if not obj.available:
                button.SetBackgroundColour( wx.Colour(128, 0, 0) )
                button.SetForegroundColour( wx.Colour(255, 255, 255) )
            sizer.Add(button, 1, wx.EXPAND | wx.ALL, 5)

    def refresh(self):
        try:
            r = rest.getMeja(serverurl=self.serverurl,
                    params=dict(results_per_page=self.objects_per_page))
        except Exception as e:
            wx.MessageBox(e.message, 'Error', wx.OK | wx.ICON_ERROR)
            logging.error(traceback.format_exc())

        #objects_num = data['num_results']
        #pages = data['total_pages']
        #objects = data['objects']
        #page = data['page']
        objects_num = r.num_results
        pages = r.total_pages
        objects = r.objects
        page = r.page

        if pages > 1:
            sizer = wx.BoxSizer(wx.HORIZONTAL)

            self.tab = wx.Notebook(self, wx.ID_ANY)
            for p in xrange(pages):
                pnl = wx.Panel(self.tab, wx.ID_ANY)
                self.tab.AddPage(pnl, '%d - %d' % 
                        ((p*self.objects_per_page+1), 
                            (p+1)*self.objects_per_page))

                pnlSizer = wx.GridSizer(0, 7, 0, 0)

                self.createButtons(objects, pnl, pnlSizer)

                fillEmptySpaces(pnlSizer, self.objects_per_page - len(objects))

                pnl.SetSizerAndFit(pnlSizer)

                try:
                    r = rest.getMeja(serverurl=self.serverurl,
                            params=dict(page=p+2, results_per_page=self.objects_per_page))
                    objects = r.objects
                except Exception as e:
                    wx.MessageBox(e.message, 'Error', wx.OK | wx.ICON_ERROR)
                    logging.error(traceback.format_exc())

            sizer.Add(self.tab, 1, wx.EXPAND, 5)
        else:
            sizer = wx.GridSizer(0, 7, 0, 0)

            self.createButtons(objects, self, sizer)

            fillEmptySpaces(sizer, self.objects_per_page - len(objects))

        self.SetSizerAndFit(sizer)
        self.Layout()


class MenuPanel(wx.Panel):
    def __init__(self, parent, serverurl='http://localhost:5000/api'):
        super(MenuPanel, self).__init__(parent)

        self.serverurl = serverurl
        self.objects_per_page = 36

        self.refresh()

    def createButtons(self, objects, parent, sizer):
        for obj in objects:
            button = wx.Button(parent, wx.ID_ANY, label=obj.nama)
            button.SetMinSize((-1,64))
            button.userdata = obj
            sizer.Add(button, 1, wx.EXPAND | wx.ALL, 5)

    def refresh(self):
        # get Menu
        try:
            r = rest.getMenu(serverurl=self.serverurl,
                    params=dict(results_per_page=self.objects_per_page))
        except Exception as e:
            wx.MessageBox(e.message, 'Error', wx.OK|wx.ICON_ERROR)
            logging.error(traceback.format_exc())

        #objects_num = data['num_results']
        #pages = data['total_pages']
        #objects = data['objects']
        #page = data['page']
        objects_num = r.num_results
        pages = r.total_pages
        objects = r.objects
        page = r.page

        sizer = wx.BoxSizer(wx.VERTICAL)

        if pages > 1:

            self.tab = wx.Notebook(self, wx.ID_ANY)
            for p in xrange(pages):
                pnl = wx.Panel(self.tab, wx.ID_ANY)
                self.tab.AddPage(pnl, '%d - %d' % 
                        ((p*self.objects_per_page+1), 
                            (p+1)*self.objects_per_page))

                pnlSizer = wx.GridSizer(0, 4, 0, 0)

                self.createButtons(objects, pnl, pnlSizer)

                fillEmptySpaces(pnlSizer, self.objects_per_page - len(objects))

                pnl.SetSizerAndFit(pnlSizer)

                try:
                    r = rest.getMenu(serverurl=self.serverurl,
                            params=dict(page=p+2, 
                                results_per_page=self.objects_per_page))
                    objects = r.objects
                except Exception as e:
                    wx.MessageBox(e.message, 'Error', wx.OK | wx.ICON_ERROR)
                    logging.error(traceback.format_exc())

            sizer.Add(self.tab, 1, wx.EXPAND, 5)
        else:
            menuSizer = wx.GridSizer(0, 4, 0, 0)

            self.createButtons(objects, self, menuSizer)
            fillEmptySpaces(menuSizer, self.objects_per_page - len(objects))
            sizer.Add(menuSizer, 1, wx.EXPAND, 5)

        self.SetSizerAndFit(sizer)
        self.Layout()


class OrderListPanel(gui.OrderListPanel):
    def __init__(self, parent, meja=None, waktu=None, server=None, pax=1, 
            items=None):
        super(OrderListPanel, self).__init__(parent)

        self.lblMeja.SetLabel('1')
        #self.lblWaktu.SetLabel(utcnow().isoformat())
        self.lblWaktu.SetLabel(utcnow().strftime('%Y-%m-%d %H:%M'))
        self.lblServer.SetLabel('david')

        self.numPax.SetFocus()

        columns = [
                ColumnDefn('Menu', 'left', 220, 'nama'),
                ColumnDefn('Qty', 'right', 50, 'qty'),
                ColumnDefn('@', 'right', 100, 'harga'),
                ColumnDefn('Subtotal', 'right', 150, 'subtotal')
                ]

        self.olvOrder.SetColumns(columns)
        self.olvOrder.rowFormatter = self.rowFormatter

        self.olvOrder.Bind(wx.EVT_LIST_INSERT_ITEM, self.onOrderChange)

        if items is not None:
            objects = copy.deepcopy(items)
            for obj in objects:
                obj.old = True
                obj.subtotal = obj.qty * obj.harga

            self.olvOrder.SetObjects(objects)

        self.numDiscount.Bind(EVT_NUM, self.onOrderChange)
        self.numTotal.Disable()
        self.numGrandTotal.Disable()
        self.olvOrder.Bind(wx.EVT_LEFT_DOWN, self.onMouseClick)

    def onMouseClick(self, event):
        print dir(event)

    def __hitungTotal(self):
        items = self.olvOrder.GetObjects()
        total = sum([x.subtotal for x in items])
        return total

    def __hitungPajak(self):
        pajak = 0.1 * self.__hitungTotal()
        return pajak

    def __hitungGrandTotal(self):
        discount = self.numDiscount.GetValue()
        grandTotal = self.__hitungTotal() - discount + self.__hitungPajak()
        return grandTotal

    def addItem(self, item):
        items = self.olvOrder.GetObjects()
        found = False

        for obj in items:
            if obj.id == item.id and not getattr(obj, 'old', False):
                obj.qty = obj.qty + item.qty
                obj.subtotal = obj.qty * obj.harga
                self.olvOrder.RefreshObject(obj)
                found = True
                self.onOrderChange(None)
                break

        if not found:
            items.append(copy.deepcopy(item))
            self.olvOrder.RepopulateList()

    def rowFormatter(self, listItem, obj):
        if getattr(obj, 'old', False):
            listItem.SetTextColour(wx.Colour(150,150,150))

    def onOrderChange(self, event):
        self.numTotal.SetValue(self.__hitungTotal())
        self.numPajak.SetValue(self.__hitungPajak())
        self.numGrandTotal.SetValue(self.__hitungGrandTotal())
