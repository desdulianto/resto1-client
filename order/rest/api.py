import requests
import functools

import copy

import logging
import traceback

def get(serverurl, path, objId=None, params=None, *args, **kwargs):
    if params is None:
        params = dict()
    
    try:
        resource = '%s/%s' % (serverurl, path)
        if objId is not None:
            resource = '%s/%s' % (resource, str(objId))
        r = requests.get(resource, params=params)
    except:
        logging.error( traceback.format_exc() )
        print 'Error: connection refused'

    if r.status_code != 200:
        raise Exception(r.status_code)

    if 'application/json' not in r.headers['content-type']:
        raise Exception('Invalid content type: %s' % r.headers['content-type'])

    return RestObject(r.json())


getMeja = functools.partial(get, path='meja')

getMenu = functools.partial(get, path='menu')

getKategori = functools.partial(get, path='kategori')

getOrder = functools.partial(get, path='order')


class RestObject(object):
    def __init__(self, rest_data):
        for key in rest_data.keys():
            if type(rest_data.get(key)) == list:
                setattr(self, key, [ RestObject(x) if type(x) == dict else x 
                    for x in rest_data.get(key) ])
            elif type(rest_data.get(key)) == dict:
                setattr(self, key, RestObject(rest_data.get(key)))
            else:
                setattr(self, key, copy.deepcopy(rest_data.get(key)))
