import wx
import logging

from gui import order

class OrderFrame(wx.Frame):
    def __init__(self, parent):
        super(OrderFrame, self).__init__(parent, wx.ID_ANY, title='Order')

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.panel = order.OrderPanel(self,serverurl='http://localhost:5000/api')

        sizer.Add(self.panel, 1, wx.EXPAND, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)


class POSOrderApp(wx.App):
    def OnInit(self):
        self.frame = OrderFrame(None)
        self.frame.ShowFullScreen(True, wx.FULLSCREEN_ALL)

        return True

if __name__ == '__main__':
    logging.basicConfig(filename='pos.log', format='%(asctime)s %(message)s', level=logging.DEBUG)
    logging.info('Start app')
    app = POSOrderApp(False)
    app.MainLoop()
    logging.info('End app')
